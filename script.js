const chartOptions = {
  width: 700,
  height: 400,
  chartPadding: {
    top: 15,
    right: 0,
    bottom: 30,
    left: 40
  },
  high: 1.1,
  low: 1.0
};

let databaseResult, data, options;

//Mixed bar with axis titles
databaseResult = [{
  batchName: 'A',
  gravity: '1.090'
}, {
  batchName: 'A',
  gravity: '1.087'
}, {
  batchName: 'A',
  gravity: '1.070'
}, {
  batchName: 'B',
  gravity: '1.050'
}, {
  batchName: 'B',
  gravity: '1.045'
}, {
  batchName: 'B',
  gravity: '1.005'
}, {
  batchName: 'C',
  gravity: '1.060'
}, {
  batchName: 'C',
  gravity: '1.037'
}, {
  batchName: 'C',
  gravity: '1.012'
}, {
  batchName: 'D',
  gravity: '1.040'
}, {
  batchName: 'D',
  gravity: '1.034'
}, {
  batchName: 'D',
  gravity: '1.015'
}];

data = formatData({
  data: databaseResult,
  chartType: 'mixedBar',
  xAxisProperty: 'batchName',
  yAxisProperty: 'gravity'
});

options = Object.assign({
  plugins: [
    Chartist.plugins.ctAxisTitle({
      axisX: {
        axisTitle: 'Batch Name',
        axisClass: 'ct-axis-title',
        offset: {
          x: 0,
          y: 30
        },
        textAnchor: 'middle'
      },
      axisY: {
        axisTitle: 'Gravity',
        axisClass: 'ct-axis-title',
        offset: {
          x: 0,
          y: -10
        },
        textAnchor: 'middle',
        flipTitle: false
      }
    })
  ]
}, chartOptions);

new Chartist.Bar('#axisTitle', data, options);

// Simple line
databaseResult = [{
  date: "2/14",
  gravity: 1.078
}, {
  date: "2/15",
  gravity: 1.067
}, {
  date: "2/16",
  gravity: 1.060
}, {
  date: "2/17",
  gravity: 1.054
}, {
  date: "2/18",
  gravity: 1.033
}];

data = formatData({
  data: databaseResult,
  chartType: 'line',
  xAxisProperty: 'date',
  yAxisProperty: 'gravity'
});

new Chartist.Line('#simpleLine', data, chartOptions);

//Mixed Bar with holes
databaseResult = [{
  batchName: '#1',
  gravity: '1.090'
}, {
  batchName: '#1',
  gravity: '1.087'
}, {
  batchName: '#2',
  gravity: '1.070'
}, {
  batchName: '#2',
  gravity: '1.050'
}, {
  batchName: '#d\\*./',
  gravity: '1.045'
}, {
  batchName: '#2',
  gravity: '1.045'
}, {
  batchName: '#1',
  gravity: '1.060'
}, {
  batchName: '#d\\*./',
  gravity: '1.037'
}, {
  batchName: '#d\\*./',
  gravity: '1.012'
}, {
  batchName: '#d\\*./',
  gravity: '1.010'
}, {
  batchName: '#2',
  gravity: '1.034'
}, {
  batchName: '#2',
  gravity: '1.015'
}];

data = formatData({
  data: databaseResult,
  chartType: 'mixedBar',
  xAxisProperty: 'batchName',
  yAxisProperty: 'gravity'
});

new Chartist.Bar('#mixedBar', data, chartOptions);
